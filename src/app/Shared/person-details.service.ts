import { Injectable } from '@angular/core';
import { PersonDetails } from './person-details.model';
import { JsonData } from './json-data.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PersonDetailsService {

  formData: PersonDetails;
  data:JsonData;
  readonly rootUrl = 'http://localhost:51691/api';

  constructor(private http: HttpClient) { }
  postPersonDetail(formData: PersonDetails) {
    return this.http.post(this.rootUrl + '/Details', formData)
  }
  postJsonData(data:JsonData) {
    return this.http.post(this.rootUrl + '/JsonTbls', data)
  } 
  getJsonData(id) {
    return this.http.get(this.rootUrl + '/JsonTbls/'+ id)
  }
}
