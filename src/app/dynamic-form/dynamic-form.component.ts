import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormGroup, FormBuilder, FormControl, FormArray, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonDetailsService } from '../Shared/person-details.service';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  styleUrls: ['./dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit, OnDestroy {
  dynamicForm: FormGroup;
  myHobbies: FormArray[]=[];
  dynamicFormArray: any;

  private routeSub: Subscription;
  constructor(private httpClient: HttpClient,
    private fb: FormBuilder,
    private router: ActivatedRoute,
    private route: Router,
    private service: PersonDetailsService,
    private toastr: ToastrService) { }

  ngOnInit() {
    this.dynamicForm = this.fb.group({
      Name: new FormControl(''),
      myHobbies: new FormArray([]),
      Hobbies: new FormControl(),
      Gender: new FormControl(''),
      Place: new FormControl('')
    });
    var id;
    this.routeSub = this.router.params.subscribe(params => {
      id = params['id'];
    });
    this.service.getJsonData(id).subscribe(
      res => {
        var a=res['JsonData'];
        this.dynamicFormArray = JSON.parse(res['JsonData']);
        console.log(this.dynamicFormArray);
      },
      err => {
        this.toastr.error(err, "Error")
      }
    )
    // this.httpClient.get('/assets/dynamic-form.json').subscribe(data => {
    //   this.dynamicFormArray = data;
    //   // this.createFormControl();
    // })

  }
 cleanString(str) {
    str = str.replace('"[', '[');
    str = str.replace(']"', ']');
    return str;
  }
  ngOnDestroy() {
    this.routeSub.unsubscribe();
  }
  // createFormControl() {
  //   this.dynamicFormArray.forEach(element => {
  //     if (element.type != "checkbox") {
  //       this.dynamicForm.addControl(element.label, new FormControl())
  //     }
  //   });

  // }

  resetForm(form?: FormGroup) {
    this.dynamicForm.reset();
    this.service.formData = {
      Id: 0,
      Name: '',
      Hobbies: '',
      Gender: '',
      Place: ''
    }
  }

  onSubmit() {
    let formArray: FormArray = this.dynamicForm.get('myHobbies') as FormArray;
    var a: String;
    for (let c of formArray.controls) {
      if (a == undefined) {
        a = c.value;
      }
      else { a = a + ',' + c.value; }

    }
    this.dynamicForm.controls['Hobbies'].setValue(a);

    console.log(this.dynamicForm.value);
    this.service.postPersonDetail(this.dynamicForm.value).subscribe(
      res => {
        this.toastr.success("Submitted Successfully", "Success")
        this.resetForm(this.dynamicForm.value);
      },
      err => {
        this.toastr.error(err, "Error")
      }
    )
    this.route.navigate(['output-form/' + JSON.stringify(this.dynamicForm.getRawValue())]);
  }

  onCheckChange(event) {
    const formArray: FormArray = this.dynamicForm.get('myHobbies') as FormArray;

    /* Selected */
    if (event.target.checked) {
      // Add a new control in the arrayForm
      formArray.push(new FormControl(event.target.value));
    }
    /* unselected */
    else {
      // find the unselected element
      let i: number = 0;

      formArray.controls.forEach((ctrl: FormControl) => {
        if (ctrl.value == event.target.value) {
          // Remove the unselected element from the arrayForm
          formArray.removeAt(i);
          return;
        }

        i++;
      });
    }
  }
}
