import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InputJsonComponent } from './input-json/input-json.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import { OutputFormComponent } from './output-form/output-form.component';
import { DynamicForm2Component } from './dynamic-form2/dynamic-form2.component';
const  routes: Routes = [
  {
    path: '',
    redirectTo: 'input-json',
    pathMatch: 'full',
  },
  {
    path: 'input-json',
    component: InputJsonComponent
  },  
  {
    path: 'dynamic-form',
    component: DynamicFormComponent
  },  
  {
    path: 'dynamic-form2',
    component: DynamicForm2Component
  }, 
  {
    path: 'output-form/:data',
    component: OutputFormComponent
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
