import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-dynamic-form2',
  templateUrl: './dynamic-form2.component.html',
  styleUrls: ['./dynamic-form2.component.scss']
})
export class DynamicForm2Component implements OnInit {
  dynamicForm: FormGroup;
  dynamicFormArray: any;
  constructor(private httpClient: HttpClient, private fb: FormBuilder,private router: ActivatedRoute,private route: Router ) { }

  ngOnInit() {
   this.dynamicForm = this.fb.group({
    });
    this.httpClient.get('/assets/dynamic-form2.json').subscribe(data => {
     this.dynamicFormArray = data;
      this.createFormControl();
    })
    
  }
  createFormControl() {
    this.dynamicFormArray.forEach(element => {
         this.dynamicForm.addControl(element.label, new FormControl())
       });
    
  }

  onSubmit() { 
    console.log(this.dynamicForm.value);
    this.route.navigate(['output-form/' + JSON.stringify(this.dynamicForm.getRawValue())]);
  }
}