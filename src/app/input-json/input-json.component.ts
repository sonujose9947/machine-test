import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { PersonDetailsService } from '../Shared/person-details.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-input-json',
  templateUrl: './input-json.component.html',
  styleUrls: ['./input-json.component.scss']
})
export class InputJsonComponent implements OnInit {
  @Input() result: string = "";
  @Output() clicked = new EventEmitter<string>();
  jsonForm = new FormGroup({
    jsonData: new FormControl('', Validators.required)
  });
  constructor(private httpClient: HttpClient, private fb: FormBuilder, private router: Router,
    private service: PersonDetailsService, private toastr: ToastrService) { }

  ngOnInit(): void {
  }
  resetForm(form?: FormGroup) {
    this.jsonForm.reset();
    this.service.data = {
      JsonId: 0,
      JsonData: ''
    }
  }
  onSubmit() {
    // var json=JSON.stringify(this.jsonForm.value.jsonData);
   //  var json=JSON.stringify(this.jsonForm.value.jsonData);
 //    this.jsonForm.controls['jsonData'].setValue(json);
    this.service.postJsonData(this.jsonForm.value).subscribe(
      res => {
        this.toastr.success("Submitted Successfully", "Success")
        console.log(res['JsonId']);
        this.router.navigate(['dynamic-form', { id: res['JsonId'] }]);
        this.resetForm(this.jsonForm.value);
      },
      err => {
        this.toastr.error(err, "Error")
      }
    )
    // console.log(this.jsonForm.value.jsonData);
  }
}
