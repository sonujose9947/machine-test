import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-output-form',
  templateUrl: './output-form.component.html',
  styleUrls: ['./output-form.component.scss']
})
export class OutputFormComponent implements OnInit {

  constructor(private httpClient: HttpClient,private router: ActivatedRoute) { }

  ngOnInit() {
    document.getElementById('textarea').innerHTML=this.router.snapshot.params['data'];
  }

}
