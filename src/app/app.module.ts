import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicFormComponent } from './dynamic-form/dynamic-form.component';
import {HttpClientModule} from '@angular/common/http'; 
import {MatRadioModule} from '@angular/material/radio';
import { InputJsonComponent } from './input-json/input-json.component';
import { OutputFormComponent } from './output-form/output-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { DynamicForm2Component } from './dynamic-form2/dynamic-form2.component';
import { PersonDetailsService } from './Shared/person-details.service';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
    DynamicFormComponent,
    InputJsonComponent,
    OutputFormComponent,
    DynamicForm2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatRadioModule,
    FormsModule,
    ReactiveFormsModule,
    MatCheckboxModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule
  ],
  providers: [PersonDetailsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
